require essioc
require adsis8300bpm

# set macros
epicsEnvSet("CONTROL_GROUP",  "PBI-BPM10")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-110")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-6")
epicsEnvSet("EVR_NAME",       "PBI-BPM10:Ctrl-EVR-101:")
epicsEnvSet("SYSTEM1_PREFIX", "HBL-160LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME",   "HBL 160 LWU BPM 01")
epicsEnvSet("SYSTEM2_PREFIX", "HBL-170LWU:PBI-BPM-001:")
epicsEnvSet("SYSTEM2_NAME",   "HBL 170 LWU BPM 01")

# load BPM
iocshLoad("$(adsis8300bpm_DIR)/bpm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

